# CSIS 10A Assignment 4 (the third assignment!)
## Getting Started
- Read: Chapter 4 
- Clone this repository using:  
```git clone https://bitbucket.org/csis10a/assignment04.git```. 

If you don't have git installed you can download this assignment at [https://bitbucket.org/csis10a/assignment04/downloads](https://bitbucket.org/csis10a/assignment04/downloads) (Click on the branches subtab)

## Assignment
### 1. Drills (8 points)

Complete the code in the Drills.java file by writing the if statements asked for.
Activate an exercise’s code block by adding a `/` at the beginning of the comment line (changing /**** to //*****). When finished with an exercise, you may deactivate the code by removing the `/` you added earler.

Each part has a description of the intended output in the comment section.

### 2. Calculator Program (2 points)

Create a new class called Calculator and do the [online Calculator Program](http://hohonuuli.bitbucket.org/csis10a/ch04/assignment04.html)

### 3. Challenge! (1 point extra credit)

The first verse of the song “99 Bottles of Beer” is:  
_99 bottles of beer on the wall, 99 bottles of beer, ya’ take one down, ya’ pass it around, 98 bottles of beer on the wall._

Subsequent verses are identical except that the number of bottles gets smaller by one in each verse, until the last verse:  
_No bottles of beer on the wall, no bottles of beer, ya’ can’t take one down, ya’ can’t pass it around, ’cause there are no more bottles of beer on the wall!_

And then the song (finally) ends.

Write a program that prints the entire lyrics of “99 Bottles of Beer.” Your program should include a recursive method that does the hard part (refer to the coundown method from lecture to guide you on the use of recursion), but you might want to write additional methods to separate the major functions of the program.

For example the phrase _N bottles of beer on the wall_ could be handled with a nonrecursive method that takes a parameter N and prints it, followed by “bottles of beer”.

As you develop your code, test it with a small number of verses, like “3 Bottles of Beer.”

The purpose of this exercise is to take a problem and break it into smaller problems, and to solve the smaller problems by writing simple methods.

## Turning in your work
When you are finished with assignment4, turn in your project folder to the homework uploader at MPC Online.
